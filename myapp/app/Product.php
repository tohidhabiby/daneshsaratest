<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Product extends Model
{
    const TABLE_NAME = 'products';
    const ID = 'id';
    const TITLE = 'title';
    const PRODUCT_TYPE = 'product_type';
    const TYPE_MAIN = 'main';
    const TYPE_ADDITIONAL = 'additional';
    const COST = 'cost';
    const PIVOT_COUNT = 'count';

    public $timestamps = false;

    public static $types = [self::TYPE_MAIN, self::TYPE_ADDITIONAL];

    /**
     * @return BelongsToMany
     */
    public function orders() : BelongsToMany
    {
        return $this->belongsToMany(Order::class)->withPivot(self::PIVOT_COUNT);
    }

    /**
     * @param string $title
     * @param string $type
     * @param int|null $cost
     * @return Product
     * @throws \Exception
     */
    public static function createFactory(string $title, string $type, ?int $cost = 0) : Product
    {
        $product = new self();
        $product->setTitle($title)
            ->setProductType($type)
            ->setCost($cost)
            ->save();
        
        return $product;
    }

    /**
     * @param string $title
     * @param string $type
     * @param int|null $cost
     * @return Product
     * @throws \Exception
     */
    public function updateFactory(string $title, string $type, ?int $cost = 0) : Product
    {
        $this->setTitle($title)
            ->setProductType($type)
            ->setCost($cost)
            ->save();
        
        return $this;
    }

    /**
     * @param Builder $builder
     * @param string $type
     * @return Builder
     */
    public function scopeWhereTypeIs(Builder $builder, string $type) : Builder
    {
        return $builder->where(self::PRODUCT_TYPE, $type);
    }

    /**
     * @param Builder $builder
     * @param int $id
     * @return Builder
     */
    public function scopeWhereIdIs(Builder $builder, int $id) : Builder
    {
        return $builder->where(self::ID, $id);
    }

    /**
     * @return int|null
     */
    public function getId() : ?int
    {
        return $this->{self::ID};
    }

    /**
     * @param int $cost
     * @return Product
     */
    public function setCost(int $cost) : Product
    {
        $this->{self::COST} = $cost;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCost() : ?int
    {
        return $this->{self::COST};
    }

    /**
     * @param string $type
     * @return Product
     * @throws \Exception
     */
    public function setProductType(string $type) : Product
    {
        if (!in_array($type, self::$types)) {
            throw new \Exception('PRODUCT_TYPE is not valid!');
        }

        $this->{self::PRODUCT_TYPE} = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProductType() : ?string
    {
        return $this->{self::PRODUCT_TYPE};
    }

    /**
     * @param string $title
     * @return Product
     */
    public function setTitle(string $title) : Product
    {
        $this->{self::TITLE} = strtolower($title);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle() : ?string
    {
        return $this->{self::TITLE};
    }
}
