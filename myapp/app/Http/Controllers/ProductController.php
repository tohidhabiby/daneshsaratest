<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index() : View
    {
        return view('products.index', ['products' => Product::paginate(20)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create() : View
    {
        return view('products.manage', ['method' => 'POST', 'route' => route('products.store')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductRequest $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function store(ProductRequest $request) : RedirectResponse
    {
        Product::createFactory($request->get('title'), $request->get('product_type'), $request->get('cost'));
        Session::flash('message', 'Product has been saved.');
        Session::flash('alert-class', 'alert-success');

        return redirect('/products');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Product $product
     * @return View
     */
    public function edit(Product $product) : View
    {
        return view(
            'products.manage',
            ['product' => $product, 'method' => 'PUT', 'route' => route('products.update', $product)]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductRequest $request
     * @param Product $product
     * @return RedirectResponse
     * @throws \Exception
     */
    public function update(ProductRequest $request, Product $product) : RedirectResponse
    {
        $product->updateFactory(
            $request->get('title'),
            $request->get('product_type'),
            $request->get('cost')
        );
        Session::flash('message', 'Product has been updated.');
        Session::flash('alert-class', 'alert-success');

        return redirect('/products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return RedirectResponse
     */
    public function destroy(Product $product) : RedirectResponse
    {
        try {
            $product->delete();
            Session::flash('message', 'Product has been deleted.');
            Session::flash('alert-class', 'alert-success');
        } catch (\Exception $exception) {
            Session::flash('message', 'Can not delete product!');
            Session::flash('alert-class', 'alert-danger');
        }

        return redirect('/products');
    }
}
