<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Order;
use App\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index() : View
    {
        return view('orders.index', ['orders' => Order::paginate(20)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create() : View
    {
        return view('orders.manage', ['method' => 'POST', 'route' => route('orders.store')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param OrderRequest $request
     * @return RedirectResponse
     */
    public function store(OrderRequest $request) : RedirectResponse
    {
        $order = Order::createFactory($request->get(OrderRequest::CUSTOMER_NAME));
        $products = [];
        foreach ($request->get('products') as $product) {
            $count = $request->has('count' . $product) ?
                $request->get('count' . $product) :
                1;
            $order->products()->attach((int)$product, [Product::PIVOT_COUNT => $count]);
            $products[] = [
                (int)$product => [

                ]
            ];
        }
        Session::flash('message', 'Order has been saved.');
        Session::flash('alert-class', 'alert-success');

        return redirect('/orders');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Order $order
     * @return View
     */
    public function edit(Order $order) : View
    {
        return view(
            'orders.manage',
            ['order' => $order, 'method' => 'PUT', 'route' => route('orders.update', $order)]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param OrderRequest $request
     * @param Order $order
     * @return RedirectResponse
     */
    public function update(OrderRequest $request, Order $order) : RedirectResponse
    {
        $order->updateFactory($request->get(OrderRequest::CUSTOMER_NAME));
        $products = [];
        foreach ($request->{OrderRequest::PRODUCTS} as $product) {
            $products[(int)$product] = [
                Product::PIVOT_COUNT => $request->has('count' . $product) ?
                    $request->get('count' . $product) :
                    1
            ];
        }
        $order->products()->sync($products);
        Session::flash('message', 'Order has been updated.');
        Session::flash('alert-class', 'alert-success');

        return redirect('/orders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Order $order
     * @return RedirectResponse
     */
    public function destroy(Order $order) : RedirectResponse
    {
        try {
            $order->delete();
            Session::flash('message', 'Order has been deleted.');
            Session::flash('alert-class', 'alert-success');
        } catch (\Exception $exception) {
            Session::flash('message', 'Can not delete order!');
            Session::flash('alert-class', 'alert-danger');
        }

        return redirect('/orders');
    }
}
