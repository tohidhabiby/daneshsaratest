<?php

namespace App\Http\Requests;

use App\Product;
use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    const CUSTOMER_NAME = 'customer_name';
    const PRODUCTS = 'products';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            self::CUSTOMER_NAME => 'required',
            self::PRODUCTS . '.*' => 'required|exists:' . Product::TABLE_NAME . ',' . Product::ID,
        ];
    }
}
