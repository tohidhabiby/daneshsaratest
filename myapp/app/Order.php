<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Order extends Model
{
    const TABLE_NAME = 'orders';
    const ID = 'id';
    const CUSTOMER_NAME = 'customer_name';

    /**
     * @return BelongsToMany
     */
    public function products() : BelongsToMany
    {
        return $this->belongsToMany(Product::class)->withPivot(Product::PIVOT_COUNT);
    }

    /**
     * @param string $customerName
     * @return Order
     */
    public static function createFactory(string $customerName) : Order
    {
        $order = new self();
        $order->setCustomerName($customerName)->save();

        return $order;
    }

    /**
     * @param string $customerName
     * @return Order
     */
    public function updateFactory(string $customerName) : Order
    {
        $this->setCustomerName($customerName)->save();

        return $this;
    }

    /**
     * @param string $name
     * @return Order
     */
    public function setCustomerName(string $name) : Order
    {
        $this->{self::CUSTOMER_NAME} = strtolower($name);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomerName() : ?string
    {
        return $this->{self::CUSTOMER_NAME};
    }
}
