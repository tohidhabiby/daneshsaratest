@extends('master')
@section('content')
    <section class="panel">
        <header class="panel-heading">
            ثبت محصول
        </header>
        <div class="panel-body">
            <form class="form-horizontal tasi-form" method="post" action="{!! $route !!}">
                @csrf
                @method($method)
                <div class="form-group">
                    <label class="col-sm-2 control-label">عنوان</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="title"
                        @if(isset($product))
                            value="{!! $product->getTitle() !!}"
                        @else
                            value=""
                        @endif
                        >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">قیمت</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="cost"
                        @if(isset($product))
                            value="{!! $product->getCost() !!}"
                        @else
                            value=""
                        @endif
                        >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">نوع محصول</label>
                    <div class="col-sm-10">
                        <select class="form-control m-bot15" name="product_type">
                            @foreach(\App\Product::$types as $type)
                                <option value="{!! $type !!}"
                                @if(isset($product) && $product->getProductType() == $type)
                                    selected="selected"
                                @endif
                                >{!! ucwords($type) !!}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <button type="submit" class="btn btn-info">ثبت</button>
            </form>
        </div>
    </section>
@endsection
