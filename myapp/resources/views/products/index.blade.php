@extends('master')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    لیست محصولات

                </header>
                <table class="table table-striped table-advance table-hover">
                    <thead>
                    <tr>
                        <th><i class="icon-bullhorn"></i>عنوان</th>
                        <th class="hidden-phone"><i class="icon-question-sign"></i>نوع محصول</th>
                        <th><i class="icon-bookmark"></i>قیمت</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td>{!! $product->getTitle() !!}</td>
                                <td class="hidden-phone">{!! $product->getProductType() !!}</td>
                                <td>{!! $product->getCost() !!}</td>
                                <td>
                                    <a type="button" class="btn btn-primary btn-xs"
                                       href="{!! route('products.edit', $product->getId()) !!}">
                                        <i class="icon-pencil"></i>
                                    </a>
                                    <form method="post" class="inline-block"
                                          action="{!! route('products.destroy', $product) !!}">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger btn-xs">
                                            <i class="icon-trash "></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </section>
        </div>
    </div>
@endsection
