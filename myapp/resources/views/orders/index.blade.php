@extends('master')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    لیست سفارشات

                </header>
                <table class="table table-striped table-advance table-hover">
                    <thead>
                    <tr>
                        <th><i class="icon-bullhorn"></i>نام مشتری</th>
                        <th class="hidden-phone"><i class="icon-question-sign"></i>تاریخ سفارش</th>
                        <th><i class="icon-bookmark"></i>قیمت کل</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <td>{!! $order->getCustomerName() !!}</td>
                            <td class="hidden-phone">{!! $order->{\App\Order::CREATED_AT} !!}</td>
                            <?php
                            $sum= 0;
                            foreach ($order->products as $product) {
                                $sum += $product->getCost() * $product->pivot->count;
                            }
                            ?>
                            <td>{!! $sum !!}</td>
                            <td>
                                <a type="button" class="btn btn-primary btn-xs"
                                   href="{!! route('orders.edit', $order) !!}">
                                    <i class="icon-pencil"></i>
                                </a>
                                <form method="post" class="inline-block"
                                      action="{!! route('orders.destroy', $order) !!}">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger btn-xs">
                                        <i class="icon-trash "></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </section>
        </div>
    </div>
@endsection
