@extends('master')
@section('content')
    <section class="panel">
        <header class="panel-heading">
            ثبت سفارش
        </header>
        <div class="panel-body">
            <form class="form-horizontal tasi-form" method="post" action="{!! $route !!}">
                @csrf
                @method($method)
                <div class="form-group">
                    <label class="col-sm-2 control-label">نام مشتری</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="customer_name"
                        @if(isset($order))
                            value="{!! $order->getCustomerName() !!}"
                            @endif>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">محصولات اصلی</label>
                    <div class="col-sm-10">
                    @foreach(\App\Product::whereTypeIs(\App\Product::TYPE_MAIN)->get() as $main)
                            <div class="row">
                                <div class="checkbox col-sm-6">
                                    <label>
                                        <input class="main" type="checkbox" name="products[]" value="{!! $main->getId() !!}"
                                              onclick="checkFunction()"
                                        @if(isset($order) && $order->products()->whereIdIs($main->getId())->first())
                                            checked="checked"
                                            @endif
                                        >
                                        {!! $main->getTitle() !!}
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    تعداد
                                    <input type="text" class="form-control" name="count{!! $main->getId() !!}"
                                           @if(isset($order) && $order->products()->whereIdIs($main->getId())->first())
                                           value="{!! $order->products()->whereIdIs($main->getId())->first()->pivot->count !!}"
                                           @else
                                           value="1"
                                           @endif
                                    >
                                </div>
                                <div class="col-sm-4"> قیمت {!! $main->getCost() !!}</div>
                            </div>
                    @endforeach
                    </div>
                </div>
                <div class="form-group" id="additionals"
                     @if(!isset($order))
                     style="display: none"
                     @endif
                >
                    <label class="col-sm-2 control-label">محصولات جانبی</label>
                    @foreach(\App\Product::whereTypeIs(\App\Product::TYPE_ADDITIONAL)->get() as $additional)
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="checkbox col-sm-6">
                                    <label>
                                        <input type="checkbox" name="products[]" value="{!! $additional->getId() !!}"
                                               @if(isset($order) && $order->products()->whereIdIs($additional->getId())->first())
                                               checked="checked"
                                            @endif
                                        >
                                        {!! $additional->getTitle() !!}
                                    </label>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" class="form-control" name="count{!! $additional->getId() !!}"
                                           @if(isset($order) && $check = $order->products()->whereIdIs($additional->getId())->first())
                                           value="{!! $check->pivot->count !!}"
                                           @else
                                           value="1"
                                        @endif
                                    >
                                </div>
                                <div class="col-sm-4"> قیمت {!! $additional->getCost() !!}</div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <button type="submit" class="btn btn-info">ثبت</button>
            </form>
        </div>
    </section>
@endsection

@section('js')
    <script type="application/javascript">
        function checkFunction() {
            if ($('.main:checked').length > 0) {
                document.getElementById("additionals").style.display = "block";
            } else {
                document.getElementById("additionals").style.display = "none";
            }
        }
    </script>
@endsection
