﻿<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="{!! asset('img/favicon.html') !!}">

    <title>Blank</title>

    <!-- Bootstrap core CSS -->
    <link href="{!! asset('css/bootstrap.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/bootstrap-reset.css') !!}" rel="stylesheet">
    <!--external css-->
    <link href="{!! asset('assets/font-awesome/css/font-awesome.css') !!}" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="{!! asset('css/style.css') !!}" rel="stylesheet">
    <link href="{!! asset('css/style-responsive.css') !!}" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="{!! asset('js/html5shiv.js') !!}"></script>
      <script src="{!! asset('js/respond.min.js') !!}"></script>
    <![endif]-->
</head>

<body>

    <section id="container" class="">
        <!--header start-->
        <header class="header white-bg">
            <div class="sidebar-toggle-box">
                <div data-original-title="Toggle Navigation" data-placement="right" class="icon-reorder tooltips"></div>
            </div>
            <!--logo start-->
            <a href="#" class="logo">دانش <span>سرا</span></a>
            <!--logo end-->
            <div class="top-nav ">
                <!--search & user info start-->
                <ul class="nav pull-right top-menu">
                    <li>
                        <input type="text" class="form-control search" placeholder="Search">
                    </li>
                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown"  href="#">
                            <img alt="" src="img/avatar1_small.jpg">
                            <span class="username">خورشیدی</span>
                        </a>
                    </li>
                    <!-- user login dropdown end -->
                </ul>
                <!--search & user info end-->
            </div>
        </header>
        <!--header end-->
        <!--sidebar start-->
        <aside>
            <div id="sidebar" class="nav-collapse ">
                <!-- sidebar menu start-->
                <ul class="sidebar-menu">
                    <li class="active">
                        <a class="" href="{!! route('home') !!}">
                            <i class="icon-dashboard"></i>
                            <span>صفحه اصلی</span>
                        </a>
                    </li>
                    <li class="sub-menu">
                        <a href="javascript:;" class="">
                            <i class="icon-book"></i>
                            <span>محصولات</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub">
                            <li><a class="" href="{!! route('products.index') !!}">لیست محصولات</a></li>
                            <li><a class="" href="{!! route('products.create') !!}">ایجاد محصول</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="" href="{!! route('orders.index') !!}">
                            <i class="icon-user"></i>
                            <span>لیست سفارشات</span>
                        </a>
                    </li>
                </ul>
                <!-- sidebar menu end-->
            </div>
        </aside>
        <!--sidebar end-->
        <!--main content start-->
        <section id="main-content">
            <section class="wrapper">
                <!-- page start-->
                @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">
                            <a href="#" style="margin-top: -15px;margin-left: -130px" class="close dataTables_filter" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{!! $error !!}</strong>
                        </div>
                    @endforeach
                @elseif(Session::has('message'))
                    <div class="alert {{ Session::get('alert-class', 'alert-info') }}">
                        <div class="nav navbar-left">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        </div>
                        {{ Session::get('message') }}
                    </div>
                {{ Session::forget('message') }}
                {{ Session::forget('alert-class') }}
            @endif

            @yield('content')
             
                <!-- page end-->
            </section>
        </section>
        <!--main content end-->
    </section>

    @yield('js')
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{!! asset('js/jquery.js') !!}"></script>
    <script src="{!! asset('js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('js/jquery.scrollTo.min.js') !!}"></script>
    <script src="{!! asset('js/jquery.nicescroll.js') !!}" type="text/javascript"></script>


    <!--common script for all pages-->
    <script src="{!! asset('js/common-scripts.js') !!}"></script>


</body>
</html>
