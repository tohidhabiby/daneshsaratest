#!/usr/bin/env sh
set -e

composer install
composer dump-autoload
php artisan key:generate
php artisan migrate:fresh --seed
